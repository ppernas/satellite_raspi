angular.module('app', [])

.config(function($httpProvider) {
    
})

.controller('mainCtrl', function($interval, $http){
	var main = this;

	main.device = {
		name: "Main Station",
		version: "0.1.0"
	}

	main.notifications = [
		"First release is ALIVE! You can see here your shits..",
		"New device added to the crew: EUROPA!",
		"One more notification...",
		"Aaaaaand the last one! This one is gonna be a little longer but don't worry!"
	]

	main.bgs = [
		{title:"Jupiter II (Europa)",url:'/images/europa.jpg'}
	]
	main.currentbg = 0;

	//GET PHOTOS FROM NASA's FEED
	$http.jsonp("http://api.flickr.com/services/feeds/photos_public.gne?id=35067687@N04&lang=en-us&format=json&jsoncallback=JSON_CALLBACK").success(function(result){
		console.log(result);

		var photos = result.items;

		for(var i in photos){
			var photo = photos[i];
			main.bgs.push({
				title: photo.title,
				url: photo.media.m.replace("_m", "_d")
			});
		}

	}).error(function(){
		main.corsError = true;
	});

	//CHECK IF inet is UP
	var checkInet = function(){
		$http.get("http://localhost:3000/inet").success(function(res){
			main.wifi = res.ssid || true;
		}).error(function(error){
			main.wifi = false;
		});

		//change bg image...
		//main.currentbg = Math.floor((Math.random() * main.bgs.length));;

		//SETUP TIME
		var currentdate = new Date();
		var hour = currentdate.getHours();
		var minutes = currentdate.getMinutes();
		if(hour < 10) hour = "0"+hour;
		if(minutes < 10) minutes = "0"+minutes;
		main.clock = hour+":"+minutes;
	};
	checkInet();

	$interval(checkInet, 10000);

})