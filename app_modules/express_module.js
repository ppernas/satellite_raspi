var express = require('express');
var cors = require('cors');
var app = express();

var server_adress;

var options = require('commander');
var pjson = require('../package.json');

options
  .version(pjson.version)
  .option('-t, --port <n>', 'TV app port', 8080)
  .option('-s, --server <n>', 'servee app port', 3000)
  .parse(process.argv);

console.log(options.port);

var port = options.server;
var port_ui = options.port;  

app.use(cors());

app.use(express.static('public')); //STATIC FILES

app.get('/', function (req, res) {
  res.send('Hello World!')
});

var os = require("os");
app.get('/ip', function(req, res){

	require('dns').lookup(os.hostname(), function (err, add, fam) {	
		res.json({ip:add, hostname:os.hostname()});
	});
	
});

var dns = require('dns');
var wifiscanner = require("node-wifiscanner");
app.get('/inet', function(req, res){
	dns.lookup('google.com',function(err) {
		if (err && err.code == "ENOTFOUND") {
            res.status(404).json(err);
        } else {

        	wifiscanner.current(function(err, data){
        		if(err){
        			res.status(404).json("wifi ssid not found");		
        		}
        		console.log(data);
        		res.status(200).json({ssid: data.SSID});	
        	});

            
        }
	})
});

var server = app.listen(port, function () {

  console.log('Express app running on port: '+port);

})


//EMULATE UI
var ui_app = express();

ui_app.use(cors());
ui_app.use(express.static('public')); //STATIC FILES
ui_app.get('/', function (req, res) {
  res.sendFile('index.html', { root: __dirname+"/../public" });
});
var server = ui_app.listen(port_ui, function(){
	console.log('Emulating user interface in port '+port_ui)
});
